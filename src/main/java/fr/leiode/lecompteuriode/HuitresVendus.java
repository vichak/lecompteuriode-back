package fr.leiode.lecompteuriode;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;

@Data
@Entity
public class HuitresVendus {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private int nbHuitres;

    @Column(unique=true)
    private Date jour;
}
