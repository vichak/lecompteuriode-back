package fr.leiode.lecompteuriode;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
class TillerOrder {
    private int id;
    private int nbCustomers;
    private List<TillerLine> lines;
}
