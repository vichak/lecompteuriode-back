package fr.leiode.lecompteuriode;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TillerAuthRequest {
    private String login;
    private String password;
    private String provider_token;
}
