package fr.leiode.lecompteuriode;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TillerProduct {
    private int id;
    private String name;
}
