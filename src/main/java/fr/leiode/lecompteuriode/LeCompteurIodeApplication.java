package fr.leiode.lecompteuriode;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeCompteurIodeApplication {

    public static void main(String[] args) {
        SpringApplication.run(LeCompteurIodeApplication.class, args);
    }

}
