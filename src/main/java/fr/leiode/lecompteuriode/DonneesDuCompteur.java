package fr.leiode.lecompteuriode;

import lombok.Data;

@Data
class DonneesDuCompteur {

    private int nbHuitresDepuisLeDebut;

    private int nbHuitresAjourdhui;

    private HuitresVendus meilleurJour;
}
