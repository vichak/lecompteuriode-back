package fr.leiode.lecompteuriode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.InetSocketAddress;
import java.net.Proxy;
import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class TillerService {

    public static DateTimeFormatter tillerFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd HH-mm-ss");
    @Autowired
    HuitresVenduRepository huitresVenduRepository;
    @Autowired
    NbHuitresDansTillerProductRepository nbHuitresDansTillerProductRepository;
    @Value("${tillerApiUrl}")
    private String tillerApiUrl;
    @Value("${tillerApiLogin}")
    private String tillerApiLogin;
    @Value("${tillerApiPassword}")
    private String tillerApiPassword;
    @Value("${tillerApiProviderToken}")
    private String tillerApiProviderToken;
    @Value("${proxy.server}")
    private String proxyServer;
    @Value("${proxy.port}")
    private String proxyPort;
    private String tillerApiRestaurantToken;
    private RestTemplate restTemplate;
    @Value("${dateOuverture}")
    private String stringDateOuverture;

    private final List<String> LINE_STATUS_TO_COUNT = Stream.of("WAITING", "IN_PROGRESS", "DONE").collect(Collectors.toList());

    public void metAjourLeCompteur() {
        log.info("DEBUT met à jour le compteur");

        LocalDate aujourdhui = LocalDate.now();

        LocalDate dateOuverture = LocalDate.parse(stringDateOuverture, tillerFormat);

        LocalDate derniereDateEnregistree = huitresVenduRepository.laDerniereDateEnregistree();
        if (derniereDateEnregistree == null) derniereDateEnregistree = dateOuverture;

        // recupère la liste des jours entre dateOuverture et la derniereDateEnregistree + 1 jour
        LocalDate start = derniereDateEnregistree.plusDays(1);
        List<LocalDate> listJours = new ArrayList<>();
        while (start.isBefore(aujourdhui)) {
            listJours.add(start);
            start = start.plusDays(1);
        }
        log.info("liste des jours à récuperer {}", listJours);

        // enregistre le nombre d'huitres vendus pour chaque jour.
        for (LocalDate jour : listJours) {
            log.info("récupération des données auprès de tiller pour {}", jour);
            HuitresVendus huitresVendus = new HuitresVendus();
            huitresVendus.setNbHuitres(this.getNbHuitresVendus(jour));
            huitresVendus.setJour(Date.valueOf(jour));
            huitresVendus = this.huitresVenduRepository.save(huitresVendus);
            log.info("huitresVendus {}", huitresVendus);
        }
        log.info("FIN met à jour le compteur");
    }

    private RestTemplate getRestTemplate() {
        if (this.restTemplate == null) {
            if (!this.proxyServer.isEmpty()) {
                SimpleClientHttpRequestFactory requestFactory = new SimpleClientHttpRequestFactory();

                Proxy proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyServer, Integer.valueOf(proxyPort)));
                requestFactory.setProxy(proxy);

                this.restTemplate = new RestTemplate(requestFactory);
            } else {
                this.restTemplate = new RestTemplate();
            }
        }
        return this.restTemplate;
    }

    public void auth() {
        TillerAuthRequest authRequest = new TillerAuthRequest(tillerApiLogin, tillerApiPassword, tillerApiProviderToken);
        HttpEntity<TillerAuthRequest> request = new HttpEntity<>(authRequest);
        TillerAuthResponse response = this.getRestTemplate().postForObject(tillerApiUrl + "/auth", request, TillerAuthResponse.class);
        this.tillerApiRestaurantToken = response.getToken();
    }

    public List<TillerOrder> getCommandes(LocalDateTime dateFrom, LocalDateTime dateTo) {
        log.info("DEBUT getCommandes from {} to {}", dateFrom, dateTo);
        if (tillerApiRestaurantToken == null) {
            this.auth();
        }

        // convertit au format tiller
        String stringDateFrom = dateFrom.format(tillerFormat);
        String stringDateTo = dateTo.format(tillerFormat);

        // Query parameters
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(tillerApiUrl + "/orders")
                // Add query parameter
                .queryParam("restaurant_token", tillerApiRestaurantToken)
                .queryParam("provider_token", tillerApiProviderToken)
                .queryParam("dateFrom", stringDateFrom)
                .queryParam("dateTo", stringDateTo);

        ResponseEntity<TillerOrdersResponse> response = this.getRestTemplate().getForEntity(builder.build().toUri(), TillerOrdersResponse.class);

        log.info("FIN getCommandes");
        return response.getBody().orders;
    }

    public int getNbHuitresVendusAujourdhui() {
        LocalDateTime now = LocalDateTime.now(ZoneId.of("Europe/Paris"));
        LocalTime midnight = LocalTime.MIDNIGHT;
        LocalDate today = LocalDate.now(ZoneId.of("Europe/Paris"));
        LocalDateTime todayMidnight = LocalDateTime.of(today, midnight);
        List<TillerOrder> orders = this.getCommandes(todayMidnight, now);
        return getNbHuitresDepuisOrders(orders);
    }

    public int getNbHuitresVendus(LocalDate jour) {
        LocalTime min = LocalTime.MIN;
        LocalDateTime jourMin = LocalDateTime.of(jour, min);
        LocalTime max = LocalTime.MAX;
        LocalDateTime jourMax = LocalDateTime.of(jour, max);
        List<TillerOrder> orders = this.getCommandes(jourMin, jourMax);
        return getNbHuitresDepuisOrders(orders);
    }

    public int getNbHuitresDepuisOrders(List<TillerOrder> orders) {
        if (orders == null) return 0;
        if (orders.isEmpty()) return 0;
        int nbHuitres = 0;
        for (TillerOrder order : orders) {
            for (TillerLine line : order.getLines()) {
                nbHuitres += getNbHuitresDepuisLine(line);
            }
        }
        return nbHuitres;
    }

    public int getNbHuitresDepuisLine(TillerLine line){
        int nbHuitres = 0;
        if (LINE_STATUS_TO_COUNT.contains(line.getStatus())) {
            nbHuitres += line.getQuantity() * this.getNbHuitresDansProduit(line.getProductId());
            // pour les formules les produits sont dans extraLines
            if (!line.getExtraLines().isEmpty()) {
                for (TillerLine extraLine : line.getExtraLines()) {
                    nbHuitres += getNbHuitresDepuisLine(extraLine);
                }
            }
        }
        return nbHuitres;
    }

    public void metAjourTillerProduct() {
        log.info("DEBUT metAjourTillerProduct");
        if (tillerApiRestaurantToken == null) {
            this.auth();
        }

        // Query parameters
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(tillerApiUrl + "/inventory")
                // Add query parameter
                .queryParam("restaurant_token", tillerApiRestaurantToken)
                .queryParam("provider_token", tillerApiProviderToken);

        ResponseEntity<TillerInventoryResponse> response = this.getRestTemplate().getForEntity(builder.build().toUri(), TillerInventoryResponse.class);
        for(TillerCategory category: response.getBody().getCategories()) {
            for (TillerProduct tillerProduct: category.getProducts()) {
                if (!nbHuitresDansTillerProductRepository.findByTillerProductId(tillerProduct.getId()).isPresent()) {
                    NbHuitresDansTillerProduct nbHuitresDansTillerProduct = new NbHuitresDansTillerProduct();
                    nbHuitresDansTillerProduct.setName(tillerProduct.getName());
                    nbHuitresDansTillerProduct.setNbHuitres(0);
                    nbHuitresDansTillerProduct.setTillerProductId(tillerProduct.getId());
                    nbHuitresDansTillerProduct = nbHuitresDansTillerProductRepository.save(nbHuitresDansTillerProduct);
                    log.info("nouveau produit créé {}", nbHuitresDansTillerProduct);
                }
            }
        }
        log.info("FIN metAjourTillerProduct");
    }

    public int getNbHuitresDansProduit(int productId) {
        Integer result = nbHuitresDansTillerProductRepository.getNbHuitresDansTillerProductByTillerProductId(productId);
        if (result == null) return 0;
        return result.intValue();
    }
}
