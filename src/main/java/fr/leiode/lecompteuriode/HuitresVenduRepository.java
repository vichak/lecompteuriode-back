package fr.leiode.lecompteuriode;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.time.LocalDate;

public interface HuitresVenduRepository extends CrudRepository<HuitresVendus, Integer> {

    @Query("select max(n.jour) from HuitresVendus n")
    LocalDate laDerniereDateEnregistree();

    @Query("select SUM(n.nbHuitres) from HuitresVendus n")
    int getNbHuitresVendus();

    HuitresVendus findFirstByOrderByNbHuitresDesc();
}
