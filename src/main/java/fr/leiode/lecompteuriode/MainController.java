package fr.leiode.lecompteuriode;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDate;

@Controller
@RequestMapping(value = "/api")
@Slf4j
public class MainController {

    @Autowired
    HuitresVenduRepository huitresVenduRepository;

    @Autowired
    private TillerService tillerService;

    @GetMapping(path = "/getDonneesDuCompteur")
    public @ResponseBody
    DonneesDuCompteur getDonneesDuCompteur() {
        log.info("DEBUT getDonneesDuCompteur");
        LocalDate derniereDateEnregistree = huitresVenduRepository.laDerniereDateEnregistree();
        LocalDate yesterday = LocalDate.now().minusDays(1);
        if (!derniereDateEnregistree.equals(yesterday)) {
            tillerService.metAjourLeCompteur();
        }
        int nbHuitresVendusAujourdhui = tillerService.getNbHuitresVendusAujourdhui();
        DonneesDuCompteur donneesDuCompteur = new DonneesDuCompteur();
        donneesDuCompteur.setNbHuitresDepuisLeDebut(huitresVenduRepository.getNbHuitresVendus() + nbHuitresVendusAujourdhui);
        donneesDuCompteur.setNbHuitresAjourdhui(nbHuitresVendusAujourdhui);
        donneesDuCompteur.setMeilleurJour(huitresVenduRepository.findFirstByOrderByNbHuitresDesc());
        log.info("FIN getDonneesDuCompteur");
        return donneesDuCompteur;
    }

    @GetMapping(path = "/metAjourLeCompteur")
    public @ResponseBody String metAjourLeCompteur() {
        log.info("DEBUT metAjourLeCompteur");
        tillerService.metAjourLeCompteur();
        log.info("FIN metAjourLeCompteur");
        return "OK";
    }

    @GetMapping(path = "/metAjourTillerProduct")
    public @ResponseBody String metAjourTillerProduct() {
        log.info("DEBUT metAjourTillerProduct");
        tillerService.metAjourTillerProduct();
        log.info("FIN metAjourTillerProduct");
        return "OK";
    }
}