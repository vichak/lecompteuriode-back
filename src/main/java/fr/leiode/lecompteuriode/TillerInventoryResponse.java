package fr.leiode.lecompteuriode;

import lombok.Data;

import java.util.List;

@Data
public class TillerInventoryResponse {
    private List<TillerCategory> categories;
}
