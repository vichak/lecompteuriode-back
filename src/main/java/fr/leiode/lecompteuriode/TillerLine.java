package fr.leiode.lecompteuriode;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
class TillerLine {
    private int id;
    private int productId;
    private int quantity;
    private List<TillerLine> extraLines;
    private String status;
    private String name;
}
