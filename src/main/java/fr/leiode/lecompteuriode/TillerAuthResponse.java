package fr.leiode.lecompteuriode;

import lombok.Data;

@Data
public class TillerAuthResponse {
    private String token;
}
