package fr.leiode.lecompteuriode;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface NbHuitresDansTillerProductRepository extends CrudRepository<NbHuitresDansTillerProduct, Integer> {
    Optional<NbHuitresDansTillerProduct> findByTillerProductId(int tillerProductId);

    @Query("select n.nbHuitres from NbHuitresDansTillerProduct n where n.tillerProductId= :tillerProductId")
    Integer getNbHuitresDansTillerProductByTillerProductId(@Param("tillerProductId") int tillerProductId);
}
