package fr.leiode.lecompteuriode;

import lombok.Data;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Data
@ToString
public class NbHuitresDansTillerProduct {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;

    private int nbHuitres;

    private int tillerProductId;

    private String name;
}
