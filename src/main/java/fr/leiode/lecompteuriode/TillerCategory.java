package fr.leiode.lecompteuriode;

import lombok.Data;
import lombok.ToString;

import java.util.List;

@Data
@ToString
public class TillerCategory {
    private int id;
    private String name;
    private List<TillerProduct> products;
}
