package fr.leiode.lecompteuriode;

import lombok.Data;

import java.util.List;

@Data
public class TillerOrdersResponse {
    List<TillerOrder> orders;
}
